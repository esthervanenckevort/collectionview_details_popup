//
//  PopupViewController.swift
//  CollectionView-Details-Popup
//
//  Created by David van Enckevort on 01-08-18.
//  Copyright © 2018 All Things Digital. All rights reserved.
//

import UIKit

class PopupViewController: UICollectionViewController {

    let cells = ["1", "2", "3", "4", "5", "6", "7", "8", "9"]

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        preferredContentSize = collectionView.collectionViewLayout.collectionViewContentSize
    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cells.count
    }

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        cell.contentView.subviews.forEach { $0.removeFromSuperview() }
        let label = UILabel(frame: .zero)
        label.text = cells[indexPath.row]
        cell.contentView.addSubview(label)
        cell.backgroundColor = .gray
        return cell
    }
}
